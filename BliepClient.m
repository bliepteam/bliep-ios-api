//
//  BliepClient.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import "BliepClient.h"
#import "Alerts.h"
#import "History.h"
#import "Credits.h"
#import "Porting.h"
#import "Profile.h"
#import "Register.h"
#import "Settings.h"
#import "SimCard.h"
#import "Help.h"
#import "PushNotificationToken.h"

static BliepClient * offlineBliepClient; // whitelisted IP address when a user has no data
static BliepClient * onlineBliepClient; // normal domain when a user has data

#ifdef DEBUG
NSString* const BliepAPIBaseUrl = @"https://staging-dot-bliep-main.appspot.com/api/";
NSString* const BliepAPIWhitelistUrl = @"https://104.155.12.78/api/";
#else
NSString* const BliepAPIBaseUrl = @"https://bliep-main.appspot.com/api/";
NSString* const BliepAPIWhitelistUrl = @"https://104.155.7.58/api/";
#endif

@interface BliepClient()

- (id)initWithCredentials:(NSString *)cid clientSecret:(NSString *)secret apiUrl:(NSString *)url;

- (void) configureJsonSerialization;
- (void) configureAuthorization;

@end

@implementation BliepClient

// parameterless accessor for the client singleton; call initWithId first to setup shared client with credentials
+ (instancetype)sharedClient {
    return [self initWithId:nil andSecret:nil];
}

+ (instancetype)initWithId:(NSString *)clientId andSecret:(NSString *)clientSecret {
    return [self initWithId:clientId andSecret:clientSecret andCertificate:nil];
}

+ (instancetype)initWithId:(NSString *)clientId andSecret:(NSString *)clientSecret andCertificate:(NSString *)certificatePath {
    static BliepClient *bliepWifiClient = nil;
    static BliepClient *bliepWwanClient = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        bliepWifiClient = [[BliepClient alloc] initWithCredentials:clientId clientSecret:clientSecret apiUrl:BliepAPIBaseUrl];
        
        bliepWwanClient = [[BliepClient alloc] initWithCredentials:clientId clientSecret:clientSecret apiUrl:BliepAPIWhitelistUrl];
        
        if (certificatePath != nil) {
            // Disable certificate pinning for now
            //            AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
            
            AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
            
            NSString *path = [[NSBundle mainBundle] pathForResource:certificatePath ofType:@"der"];
            NSData *data = [NSData dataWithContentsOfFile:path];
            policy.allowInvalidCertificates = YES;
            policy.validatesCertificateChain = NO;
            policy.validatesDomainName = NO;
            if (data != nil) {
                [policy setPinnedCertificates:@[data]];
            }
            
            bliepWwanClient.securityPolicy = policy;
        }
    });
    
    // Start monitoring for changes to reachability in order to use correct client
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    if ([[AFNetworkReachabilityManager sharedManager] isReachableViaWiFi]) {
        return bliepWifiClient;
    } else {
        return bliepWwanClient;
    }
    
}

- (id)initWithCredentials:(NSString *)cid clientSecret:(NSString *)secret apiUrl:(NSString *)url {
    self = [super initWithBaseURL:[NSURL URLWithString:url]];
    if (!self) {
        return nil;
    }
    self.clientId = cid;
    self.clientSecret = secret;
    
    [self configureJsonSerialization];
    [self configureAuthorization];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    return self;
}

- (void) configureJsonSerialization {
    // force json serialization
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    
    // accept all possible json formats. note: text/html is allowed to handle an AF networking deserialization bug where the content type is not correctly interpreted.
    self.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    
    [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type" ];
}

- (void) configureAuthorization {
    // load any existing credentials from keychain
    [self setAuthorizationHeaderWithCredentials: [self getCredentials]];
}

- (NSURLSessionDataTask *)viewProfile:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Profile viewProfile:self success:success failure:failure];
}

- (NSURLSessionDataTask *)viewReferral:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Profile viewReferral:self success:success failure:failure];
    
}

- (NSURLSessionDataTask *)viewSignature:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Profile viewSignature:self success:success failure:failure];
    
}

- (NSURLSessionDataTask *)updateSignature:(NSString *)signature
                                      use:(NSString *)use
                                  success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                  failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Profile updateSignature:self signature:signature use:use success:success failure:failure];
}

- (NSURLSessionDataTask *)updateProfile:(NSString *)email
                            oldPassword:(NSString *)oldPassword
                            newPassword:(NSString *)newPassword
                                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [Profile updateProfile:self email:email oldPassword:oldPassword newPassword:newPassword success:success failure:failure];
}

- (NSURLSessionDataTask *)activateBundle:(NSString *)bundleName
                                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [Profile activateBundle:self bundleName:bundleName success:success failure:failure];
}

- (NSURLSessionDataTask *)listHistory:(NSDate *)endDate
                              success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [History listHistory:self endDate:endDate success:success failure:failure];
}

- (NSURLSessionDataTask *)listCreditHistory:(NSNumber *)limit
                                     offset:(NSNumber *)offset
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Credits listCreditHistory:self limit:limit offset:offset success:success failure:failure];
}

- (NSURLSessionDataTask *)shareCredit:(NSDecimalNumber *)amount
                               msisdn:(NSString *)msisdn
                          addressbook:(NSString *)addressbook
                      personalMessage:(NSString *)message
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Credits shareCredit:self amount:amount msisdn:msisdn addressbook:addressbook personalMessage:message
                        success:success failure:failure];
}

- (NSURLSessionDataTask *)listSharingAddressBook:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Credits listSharingAddressBook:self success:success failure:failure];
}

- (NSURLSessionDataTask *)topupCredit:(NSDecimalNumber *)amount
                          redirectURL:(NSString *)url
                            recurring:(NSString *)recurring
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Credits topupCredit:self withAmount:amount redirectURL:url recurring:recurring success:success failure:failure];
}

- (NSURLSessionDataTask *)topupVoucherCredit:(NSString *)voucherCode
                                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Credits topupVoucherCredit:self withVoucherCode:voucherCode success:success failure:failure];
}


- (NSURLSessionDataTask *)addData:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Credits addData:self success:success failure:failure];
}

- (NSURLSessionDataTask *)listPortingProviders:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Porting listPortingProviders:self success:success failure:failure];
}

- (NSURLSessionDataTask *)createPortIn:(NSString *)provider
                                 iccid:(NSString *)iccid
                                msisdn:(NSString *)msisdn
                          contractType:(NSNumber *)contract
                               success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Porting createPortIn:self provider:provider iccid:iccid msisdn:msisdn contractType:contract success:success failure:failure];
}

- (NSURLSessionDataTask *)createAccount:(NSString *)name
                                  email:(NSString *)email
                               password:(NSString *)password
                                    sex:(NSString *)sex
                              birthdate:(NSDate *)birthdate
                                 msisdn:(NSString *)msisdn
                                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Register createAccount:self username:name email:email password:password sex:sex birthdate:birthdate msisdn:msisdn success:success failure:failure];
}

- (NSURLSessionDataTask *)confirmAccount:(NSString *)code
                                  msisdn:(NSString *)msisdn
                                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Register confirmAccount:self confirmationCode:code msisdn:msisdn success:success failure:failure];
}

- (NSURLSessionDataTask *)forgotLogin:(NSString *)email
                               msisdn:(NSString *)msisdn
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [Register forgotLogin:self email:email msisdn:msisdn success:success failure:failure];
}

- (NSURLSessionDataTask *)viewSettings:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Settings viewSettings:self success:success failure:failure];
}

- (NSURLSessionDataTask *)updateSettings:(BOOL)callFromBalance
                         voicemailActive:(BOOL)voicemailActive
                  lowBalanceNotification:(BOOL)lowBalanceNotification
                                 success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Settings updateSettings:self callFromBalance:callFromBalance voicemailActive:voicemailActive lowBalanceNotification:lowBalanceNotification success:success failure:failure];
}

- (NSURLSessionDataTask *)viewRoamingSettings:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Settings viewRoamingSettings:self success:success failure:failure];
}

- (NSURLSessionDataTask *)updateRoamingSettings:(BOOL)freeEUUse
                                  freeEUGPRSUse:(BOOL)freeEUGPRSUse
                              dayInternetBundle:(BOOL)dayInternetBundle
                             weekInternetBundle:(BOOL)weekInternetBundle
                               roamingSmsBundle:(BOOL)roamingSmsBundle
                                        success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [Settings updateRoamingSettings:self freeEUUse:freeEUUse freeEUGPRSUse:freeEUGPRSUse dayInternetBundle:dayInternetBundle weekInternetBundle:weekInternetBundle roamingSmsBundle:roamingSmsBundle success:success failure:failure];
}

- (NSURLSessionDataTask *)viewPaymentSettings:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Settings viewPaymentSettings:self success:success failure:failure];
}


- (NSURLSessionDataTask *)updatePaymentSettings:(BOOL)enableMonthly
                                  monthlyAmount:(NSNumber *)monthlyAmount
                                enableLowCredit:(BOOL)enableLowCredit
                                lowCreditAmount:(NSNumber *)lowCreditAmount
                                        success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [Settings updatePaymentSettings:self
                             enableMonthly:enableMonthly
                             monthlyAmount:monthlyAmount
                           enableLowCredit:enableLowCredit
                           lowCreditAmount:lowCreditAmount
                                   success:success
                                   failure:failure];
}

- (NSURLSessionDataTask *)disablePaymentSettings:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Settings disablePaymentSettings:self success:success failure:failure];
}

- (NSURLSessionDataTask *)updateSimCardState:(BOOL)serviceOn
                                      plusOn:(BOOL)plusOn
                                     voiceOn:(BOOL)voiceOn
                                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [SimCard updateSimCardState:self serviceOn:serviceOn plusOn:plusOn voiceOn:voiceOn success:success failure:failure];
}

- (NSURLSessionDataTask *)listFAQ:(NSInteger)categoryId
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Help listFAQ:self category:categoryId success:success failure:failure];
}

- (NSURLSessionDataTask *)searchFAQ:(NSString *)text
                            success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Help searchFAQ:self text:text success:success failure:failure];
}

- (NSURLSessionDataTask *)askQuestion:(NSString *)name
                                email:(NSString *)email
                              message:(NSString *)message
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [Help askQuestion:self name:name email:email message:message success:success failure:failure];
}

- (NSURLSessionDataTask *)viewAlertMessage:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [Alerts viewAlertMessage:self success:success failure:failure];
}

- (NSURLSessionDataTask *)viewPushNotificationToken:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return [PushNotificationToken viewPushNotificationToken:self success:success failure:failure];
}

- (NSURLSessionDataTask *)savePushNotificationToken:(NSString *)token
                                            version:(NSString *)version
                                            success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [PushNotificationToken savePushNotificationToken:self
                                                      token:token
                                                    version:version
                                                    success:success
                                                    failure:failure];
}

@end
