
//
//  History.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 11/05/15.
//  Copyright (c) 2015 *bliep. All rights reserved.
//

#import "BliepClient.h"
#import "History.h"

@implementation History

+ (NSURLSessionDataTask *)listHistory:(BliepClient *)client
                              endDate:(NSDate *)date
                              success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSDictionary *parameters;
    
    if (date) {
        // Note: time interval is in seconds but API epxects milliseconds
        NSNumber *ts = [NSNumber numberWithDouble:[date timeIntervalSince1970] * 1000];
        parameters = [NSDictionary dictionaryWithObject:ts forKey:@"end_date"];
    } else {
        parameters = [[NSDictionary alloc] init];
    }
    
    
    return [client GET:@"history" parameters:parameters success:success failure:failure];
}

@end
