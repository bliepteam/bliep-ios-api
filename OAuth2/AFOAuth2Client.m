//
//  AFOAuth2Client.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import "AFOAuth2Client.h"
#import "AFOAuthCredentials.h"

NSString * const AFOAuthCodeGrantType = @"authorization_code";
NSString * const kAFOAuthClientCredentialsGrantType = @"client_credentials";
NSString * const kAFOAuthResourceOwnerGrantType = @"password";
NSString * const kAFOAuthRefreshGrantType = @"refresh_token";

@interface AFOAuth2Client()

- (void)setAuthorizationHeaderWithCredentials:(AFOAuthCredentials *)credentials;
- (void)setBuildVersionHeader;
- (NSDictionary *)clientParametersWithDictionary:(NSDictionary *)parameters;
- (void)authenticate:(NSString *)path
          parameters:(NSDictionary *)parameters
             success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
             failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

@property (readwrite, nonatomic) NSString *_accessToken;
@property (readwrite, nonatomic) NSString *_clientAccessToken;

- (NSString *) accessToken;
- (NSString *) clientAccessToken;

- (BOOL) isTokenExpiredError:(NSError *)error;

@end

@implementation AFOAuth2Client

@synthesize clientId;
@synthesize clientSecret;
@synthesize redirectUri;
@synthesize oauthError;

- (void)setAuthorizationHeaderWithCredentials:(AFOAuthCredentials *)credentials {
    NSString *token = [NSString stringWithFormat:@"Bearer %@", credentials.accessToken];
    [self.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    // Set client build header to identify individual releases
    [self setBuildVersionHeader];
}

- (void)setBuildVersionHeader {
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    NSString *versionBuild = version;
    if (![version isEqualToString: build]) {
        versionBuild = build;
    }
    
    NSString *header = [NSString stringWithFormat: @"IOS-%@", versionBuild];
    [self.requestSerializer setValue:header forHTTPHeaderField:@"BliepApp"];
    
}

- (NSDictionary *)clientParametersWithDictionary:(NSDictionary *)parameters {
    NSMutableDictionary *clientParameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                             self.clientId, @"client_id",
                                             self.clientSecret, @"client_secret",
                                             nil];
    
    [clientParameters addEntriesFromDictionary:parameters];
    return clientParameters;
}

- (void)authenticateResourceOwner:(NSString *)username
                         password:(NSString *)password
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       kAFOAuthResourceOwnerGrantType, @"grant_type", nil];
    [parameters setValue:username forKey:@"username"];
    [parameters setValue:password forKey:@"password"];
    NSDictionary *params = [self clientParametersWithDictionary:parameters];
    
    [self authenticate:@"token" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        AFOAuthCredentials* credentials = (AFOAuthCredentials *)responseObject;
        [AFOAuthCredentials storeCredentials:credentials withIdentifier:kAFOAuthResourceOwnerGrantType];
        
        if (success) success(task, responseObject);
    } failure:failure];
}

- (void)authenticateClientCredentials:
(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       kAFOAuthClientCredentialsGrantType, @"grant_type", nil];
    NSDictionary *params = [self clientParametersWithDictionary:parameters];
    
    [self authenticate:@"token" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        AFOAuthCredentials* credentials = (AFOAuthCredentials *)responseObject;
        [AFOAuthCredentials storeCredentials:credentials withIdentifier:kAFOAuthClientCredentialsGrantType];
        
        self._clientAccessToken = credentials.accessToken;
        
        if (success) success(task, responseObject);
    } failure:failure];
}

- (void)authenticateRefreshToken:(NSString *)refreshToken
                         success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       kAFOAuthRefreshGrantType, @"grant_type", nil];
    NSDictionary *params = [self clientParametersWithDictionary:parameters];
    
    [self authenticate:@"token" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if (success) success(task, responseObject);
    } failure:failure];
}

- (void)authenticate:(NSString *)path
          parameters:(NSDictionary *)parameters
             success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
             failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSString* const oauthBaseUrl = [NSString stringWithFormat:@"%@oauth2/%@", self.baseURL.absoluteString, path];
    
    [self POST:oauthBaseUrl parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        AFOAuthCredentials *credentials = [[AFOAuthCredentials alloc] init];
        
        NSString *refreshToken = [responseObject valueForKey:@"refresh_token"];
        if (refreshToken == nil || [refreshToken isEqual:[NSNull null]]) {
            credentials.refreshToken = [parameters valueForKey:@"refresh_token"];
        }
        credentials.accessToken = [responseObject valueForKey:@"access_token"];
        credentials.tokenType = [parameters valueForKey:@"grant_type"];
        
        // update client's credentials
        [self setAuthorizationHeaderWithCredentials: credentials];
        
        if (success) success(task, credentials);
    }
       failure:failure];
}

- (void)deauthenticateResourceOwner {
    NSString* url = [NSString stringWithFormat:@"%@oauth2/revoke", self.baseURL.absoluteString];

    // fire and forget
    [self POST:url parameters:@{} success:nil failure:nil];
    
    [AFOAuthCredentials deleteCredentialsWithIdentifier:kAFOAuthResourceOwnerGrantType];
    self._accessToken = nil;
}

- (void)deauthenticateClient {
    [AFOAuthCredentials deleteCredentialsWithIdentifier:kAFOAuthClientCredentialsGrantType];
    self._clientAccessToken = nil;
}

- (AFOAuthCredentials *) getCredentials {
    AFOAuthCredentials *credentials = [AFOAuthCredentials retrieveCredentialsWithIdentifier:kAFOAuthResourceOwnerGrantType];
    if (!credentials.isValid) {
        credentials = [AFOAuthCredentials retrieveCredentialsWithIdentifier:kAFOAuthClientCredentialsGrantType];
    }
    return credentials;
}

- (NSString *) accessToken {
    if (self._accessToken == nil || [self._accessToken isEqual:[NSNull null]]) {
        AFOAuthCredentials *credentials = [AFOAuthCredentials retrieveCredentialsWithIdentifier:kAFOAuthResourceOwnerGrantType];
        self._accessToken = credentials.accessToken;
    }
    return self._accessToken;
}

- (NSString *) clientAccessToken {
    if (self._clientAccessToken == nil || [self._clientAccessToken isEqual:[NSNull null]]) {
        AFOAuthCredentials *credentials = [AFOAuthCredentials retrieveCredentialsWithIdentifier:kAFOAuthClientCredentialsGrantType];
        self._clientAccessToken = credentials.accessToken;
    }
    return self._clientAccessToken;
}

- (BOOL) isResourceOwnerAuthorized {
    return [self accessToken] != nil;
}

- (BOOL) isClientAuthorized {
    return [self clientAccessToken] != nil;
}

- (BOOL) isTokenExpiredError:(NSError *)error {
    // TODO: check on error code instead of arbitrary magic string
    NSString *err = error.localizedDescription;
    
    if ([err rangeOfString:@"access token is invalid"].location == NSNotFound &&
        [err rangeOfString:@"Je login is niet meer geldig"].location == NSNotFound &&
        [err rangeOfString:@"customer before"].location == NSNotFound) {
        return NO;
    } else {
        return YES;
    }
}

- (NSURLSessionDataTask *)GET:(NSString *)URLString
                   parameters:(NSDictionary *)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    // wrap http method with oauth error handling
    return [super GET:URLString parameters:parameters success:success failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (failure && self.oauthError && [self isTokenExpiredError:error]) {
            self.oauthError(task, error);
        } else if (failure) {
            failure(task, error);
        }
    }];
}

- (NSURLSessionDataTask *)PUT:(NSString *)URLString
                   parameters:(NSDictionary *)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    // wrap http method with oauth error handling
    return [super PUT:URLString parameters:parameters success:success failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (failure && self.oauthError && [self isTokenExpiredError:error]) {
            self.oauthError(task, error);
        } else if (failure) {
            failure(task, error);
        }
    }];
}

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(NSDictionary *)parameters
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    // wrap http method with oauth error handling
    return [super POST:URLString parameters:parameters success:success failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (failure && self.oauthError && [self isTokenExpiredError:error]) {
            self.oauthError(task, error);
        } else if (failure) {
            failure(task, error);
        }
    }];
}


@end
