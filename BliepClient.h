//
//  BliepClient.h
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import "AFOAuth2Client.h"

@interface BliepClient : AFOAuth2Client

@property (nonatomic,strong)NSString *clientId;
@property (nonatomic,strong)NSString *clientSecret;
@property (nonatomic,strong)NSString *redirectUri;

+ (instancetype)sharedClient;
+ (instancetype)initWithId:(NSString *)clientId andSecret:(NSString *)clientSecret;
+ (instancetype)initWithId:(NSString *)clientId andSecret:(NSString *)clientSecret andCertificate:(NSString *)certificatePath;

typedef NS_ENUM(NSInteger, BliepFAQCategory) {
    BliepFAQCategoryPersonalData = 72007,
    BliepFAQCategoryCreditHistory = 73004,
    BliepFAQCategorySignature = 74007,
    BliepFAQCategoryParents = 75006,
    BliepFAQCategoryGeneral = 123001,
    BliepFAQCategoryOrdering = 124001,
    BliepFAQCategoryUpgrading = 125001,
    BliepFAQCategorySharing = 126001,
    BliepFAQCategorySelfCare = 127001,
    BliepFAQCategoryNumberPorting = 193036,
};

// Wrapping invocation of API classes

- (NSURLSessionDataTask *)viewProfile:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)viewReferral:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)viewSignature:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)updateSignature:(NSString *)signature
                                      use:(NSString *)use
                                  success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                  failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)updateProfile:(NSString *)email
                            oldPassword:(NSString *)oldPassword
                            newPassword:(NSString *)newPassword
                                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)activateBundle:(NSString *)bundleName
                                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


- (NSURLSessionDataTask *)listHistory:(NSDate *)endDate
                              success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)listCreditHistory:(NSNumber *)limit
                                     offset:(NSNumber *)offset
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)shareCredit:(NSDecimalNumber *)amount
                               msisdn:(NSString *)msisdn
                          addressbook:(NSString *)addressbook
                      personalMessage:(NSString *)message
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)listSharingAddressBook:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)topupCredit:(NSDecimalNumber *)amount
                          redirectURL:(NSString *)url
                            recurring:(NSString *)recurring
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)topupVoucherCredit:(NSString *)voucherCode
                                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)addData:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)listPortingProviders:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)createPortIn:(NSString *)provider
                                 iccid:(NSString *)iccid
                                msisdn:(NSString *)msisdn
                          contractType:(NSNumber *)contract
                               success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)createAccount:(NSString *)name
                                  email:(NSString *)email
                               password:(NSString *)password
                                    sex:(NSString *)sex
                              birthdate:(NSDate *)birthdate
                                 msisdn:(NSString *)msisdn
                                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)confirmAccount:(NSString *)code
                                  msisdn:(NSString *)msisdn
                                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)forgotLogin:(NSString *)email
                               msisdn:(NSString *)msisdn
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)viewSettings:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)updateSettings:(BOOL)callFromBalance
                         voicemailActive:(BOOL)voicemailActive
                  lowBalanceNotification:(BOOL)lowBalanceNotification
                                 success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)viewRoamingSettings:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)updateRoamingSettings:(BOOL)freeEUUse
                                  freeEUGPRSUse:(BOOL)freeEUGPRSUse
                              dayInternetBundle:(BOOL)dayInternetBundle
                             weekInternetBundle:(BOOL)weekInternetBundle
                               roamingSmsBundle:(BOOL)roamingSmsBundle
                                        success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)viewPaymentSettings:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)updatePaymentSettings:(BOOL)enableMonthly
                                  monthlyAmount:(NSNumber *)monthlyAmount
                                enableLowCredit:(BOOL)enableLowCredit
                                lowCreditAmount:(NSNumber *)lowCreditAmount
                                        success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)disablePaymentSettings:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)updateSimCardState:(BOOL)serviceOn
                                      plusOn:(BOOL)plusOn
                                     voiceOn:(BOOL)voiceOn
                                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)listFAQ:(NSInteger)categoryId
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)searchFAQ:(NSString *)text
                            success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)askQuestion:(NSString *)name
                                email:(NSString *)email
                              message:(NSString *)message
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


- (NSURLSessionDataTask *)viewAlertMessage:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)viewPushNotificationToken:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)savePushNotificationToken:(NSString *)token
                                            version:(NSString *)version
                                            success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
@end
