//
//  Porting.h
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BliepClient;

@interface Porting : NSObject

+ (NSURLSessionDataTask *)listPortingProviders:(BliepClient *)client
                                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)createPortIn:(BliepClient *)client
                              provider:(NSString *)provider
                                 iccid:(NSString *)iccid
                                msisdn:(NSString *)msisdn
                          contractType:(NSNumber *)contract
                               success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

@end
