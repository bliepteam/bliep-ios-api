//
//  Credits.h
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BliepClient;

@interface Credits : NSObject

+ (NSURLSessionDataTask *)listCreditHistory:(BliepClient *)client
                                      limit:(NSNumber *)limit
                                     offset:(NSNumber *)offset
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)shareCredit:(BliepClient *)client
                               amount:(NSDecimalNumber *)amount
                               msisdn:(NSString *)msisdn
                          addressbook:(NSString *)addressbook
                      personalMessage:(NSString *)message
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)listSharingAddressBook:(BliepClient *)client
                                         success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)topupCredit:(BliepClient *)client
                           withAmount:(NSDecimalNumber *)amount
                          redirectURL:(NSString *)url
                            recurring:(NSString *)recurring
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)topupVoucherCredit:(BliepClient *)client
                             withVoucherCode:(NSString *)voucherCode
                                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)addData:(BliepClient *)client
                                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

@end
