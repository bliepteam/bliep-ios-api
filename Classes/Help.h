//
//  Help.h
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BliepClient;

@interface Help : NSObject

+ (NSURLSessionDataTask *)listFAQ:(BliepClient *)client
                         category:(NSInteger)categoryId
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)searchFAQ:(BliepClient *)client
                               text:(NSString *)text
                            success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)askQuestion:(BliepClient *)client
                                 name:(NSString *)name
                                email:(NSString *)email
                              message:(NSString *)message
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


@end
