//
//  PushNotificationToken.h
//  BliepIOSAPI
//
//  Created by Travis Dunn on 15/06/15.
//  Copyright (c) 2015 *bliep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BliepClient;

@interface PushNotificationToken : NSObject

+ (NSURLSessionDataTask *)viewPushNotificationToken:(BliepClient *)client
                               success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)savePushNotificationToken:(BliepClient *)client
                                 token:(NSString *)token
                                version:(NSString *)version
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


@end
