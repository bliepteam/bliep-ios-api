//
//  Register.h
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/17/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BliepClient;

@interface Register : NSObject

+ (NSURLSessionDataTask *)createAccount:(BliepClient *)client
                               username:(NSString *)name
                                  email:(NSString *)email
                               password:(NSString *)password
                                    sex:(NSString *)sex
                              birthdate:(NSDate *)birthdate
                                 msisdn:(NSString *)msisdn
                                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)confirmAccount:(BliepClient *)client
                        confirmationCode:(NSString *)code
                                  msisdn:(NSString *)msisdn
                                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)forgotLogin:(BliepClient *)client
                                email:(NSString *)email
                               msisdn:(NSString *)msisdn
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
@end
