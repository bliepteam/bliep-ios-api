//
//  PushNotificationToken.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 15/06/15.
//  Copyright (c) 2015 *bliep. All rights reserved.
//

#import "PushNotificationToken.h"
#import "BliepClient.h"

@implementation PushNotificationToken

+ (NSURLSessionDataTask *)viewPushNotificationToken:(BliepClient *)client
                                            success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [client GET:@"push_notification_token" parameters:NULL success:success failure:failure];
}


+ (NSURLSessionDataTask *)savePushNotificationToken:(BliepClient *)client
                                              token:(NSString *)token
                                            version:(NSString *)version
                                            success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                token, @"token",
                                version ? version : [NSNull null], @"version",
                                nil];
    
    return [client POST:@"push_notification_tokens" parameters:parameters success:success failure:failure];
    
    
    
    
}

@end
