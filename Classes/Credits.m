//
//  Credits.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import "Credits.h"
#import "BliepClient.h"

@implementation Credits

+ (NSURLSessionDataTask *)listCreditHistory:(BliepClient *)client
                                      limit:(NSNumber *)limit
                                     offset:(NSNumber *)offset
                                    success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:limit, @"limit", offset, @"offset", nil];
    
    return [client GET:@"profile/credit/history" parameters:parameters success:success failure:failure];
}

+ (NSURLSessionDataTask *)shareCredit:(BliepClient *)client
                               amount:(NSDecimalNumber *)amount
                               msisdn:(NSString *)msisdn
                          addressbook:(NSString *)addressbook
                      personalMessage:(NSString *)message
                              success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:amount, @"amount",
                                msisdn ? msisdn : [NSNull null], @"msisdn",
                                addressbook ? addressbook : [NSNull null], @"addressbook",
                                message ? message : [NSNull null], @"personal_message", nil];
    
    return [client POST:@"profile/credit/share" parameters:parameters success:success failure:failure];
}

+ (NSURLSessionDataTask *)listSharingAddressBook:(BliepClient *)client
                                         success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [client GET:@"profile/credit/share/addressbook" parameters:nil success:success failure:failure];
}

+ (NSURLSessionDataTask *)topupCredit:(BliepClient *)client
                           withAmount:(NSDecimalNumber *)amount
                          redirectURL:(NSString *)url
                            recurring:(NSString *)recurring
                              success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                amount, @"amount",
                                url, @"redirect_url",
                                recurring ? recurring : [NSNull null], @"recurring",
                                nil];
    
    return [client POST:@"profile/credit/web" parameters:parameters success:success failure:failure];
}

+ (NSURLSessionDataTask *)topupVoucherCredit:(BliepClient *)client
                             withVoucherCode:(NSString *)voucherCode
                                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObject:voucherCode forKey:@"voucher_code"];
    
    return [client POST:@"profile/credit/voucher" parameters:parameters success:success failure:failure];
}

+ (NSURLSessionDataTask *)addData:(BliepClient *)client
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [client PUT:@"profile/balance/data" parameters:NULL success:success failure:failure];
    
}


@end
