//
//  Settings.m
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import "Settings.h"
#import "BliepClient.h"

@implementation Settings

+ (NSURLSessionDataTask *)viewSettings:(BliepClient *)client
                               success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [client GET:@"profile/settings" parameters:NULL success:success failure:failure];
}

+ (NSURLSessionDataTask *)updateSettings:(BliepClient *)client
                         callFromBalance:(BOOL)callFromBalance
                         voicemailActive:(BOOL)voicemailActive
                  lowBalanceNotification:(BOOL)lowBalanceNotification
                                 success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                @(callFromBalance), @"call_from_balance",
                                @(voicemailActive), @"voicemail_active",
                                @(lowBalanceNotification), @"low_balance_notification", nil];
    
    return [client PUT:@"profile/settings" parameters:parameters success:success failure:failure];
}

+ (NSURLSessionDataTask *)viewRoamingSettings:(BliepClient *)client
                                      success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [client GET:@"profile/settings/roaming" parameters:NULL success:success failure:failure];
}

+ (NSURLSessionDataTask *)updateRoamingSettings:(BliepClient *)client
                                      freeEUUse:(BOOL)freeEUUse
                                  freeEUGPRSUse:(BOOL)freeEUGPRSUse
                              dayInternetBundle:(BOOL)dayInternetBundle
                             weekInternetBundle:(BOOL)weekInternetBundle
                               roamingSmsBundle:(BOOL)roamingSmsBundle
                                        success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                @(freeEUUse), @"free_eu_use",
                                @(freeEUGPRSUse), @"free_eu_internet",
                                @(dayInternetBundle), @"day_internet_bundle",
                                @(weekInternetBundle), @"week_internet_bundle",
                                @(roamingSmsBundle), @"sms_bundle", nil];
    
    return [client PUT:@"profile/settings/roaming" parameters:parameters success:success failure:failure];
}

+ (NSURLSessionDataTask *)viewPaymentSettings:(BliepClient *)client
                                      success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [client GET:@"profile/settings/recurring_payment" parameters:NULL success:success failure:failure];
}

+ (NSURLSessionDataTask *)updatePaymentSettings:(BliepClient *)client
                                  enableMonthly:(BOOL)enableMonthly
                                  monthlyAmount:(NSNumber *)monthlyAmount
                                enableLowCredit:(BOOL)enableLowCredit
                                lowCreditAmount:(NSNumber *)lowCreditAmount
                                        success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *monthlyParameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @(enableMonthly), @"enabled",
                                       monthlyAmount, @"amount", nil];
    NSDictionary *lowCreditParameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                         @(enableLowCredit), @"enabled",
                                         lowCreditAmount, @"amount", nil];
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                monthlyParameters, @"monthly",
                                lowCreditParameters, @"lowcredit", nil];
    
    return [client PUT:@"profile/settings/recurring_payment" parameters:parameters success:success failure:failure];
}

+ (NSURLSessionDataTask *)disablePaymentSettings:(BliepClient *)client
                                         success: (void (^)(NSURLSessionDataTask *task, id responseObject))success
                                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    return [client DELETE:@"profile/settings/recurring_payment" parameters:NULL success:success failure:failure];
}

@end
