//
//  Profile.h
//  BliepIOSAPI
//
//  Created by Travis Dunn on 10/3/13.
//  Copyright (c) 2013 *bliep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BliepClient;

@interface Profile : NSObject

+ (NSURLSessionDataTask *)viewProfile:(BliepClient *)client
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)viewReferral:(BliepClient *)client
                               success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)viewSignature:(BliepClient *)client
                                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)updateSignature:(BliepClient *)client
                                signature:(NSString *)signature
                                      use:(NSString *)use
                                  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                  failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)updateProfile:(BliepClient *)client
                                  email:(NSString *)email
                            oldPassword:(NSString *)oldPassword
                            newPassword:(NSString *)newPassword
                                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (NSURLSessionDataTask *)activateBundle:(BliepClient *)client
                              bundleName:(NSString *)bundleName
                                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
    
@end
